import {ref, set, onValue} from "https://www.gstatic.com/firebasejs/10.4.0/firebase-database.js";
import {db} from './conn.js'

function display(){
  const dbRef=ref(db,'productos');
  onValue(dbRef,(snaphot) => {
    snaphot.forEach((childSnapshot) => {
      const data = childSnapshot.val(); 

      const div = document.createElement('div');
      div.className = 'card';
      div.style.width = '18rem';
      div.style.marginTop = '20px';

      const img = document.createElement('img');
      img.src = data.foto; // Reemplaza 'producto.imagenURL' con la URL de la imagen del producto
      img.className = 'card-img-top';
      img.alt = data.nombre; // Reemplaza 'producto.nombre' con el nombre del producto
      img.style.height = '250px';

      const cardBody = document.createElement('div');
      cardBody.className = 'card-body';

      const cardTitle = document.createElement('h5');
      cardTitle.className = 'card-title';
      cardTitle.textContent = data.nombre; // Reemplaza 'producto.nombre' con el nombre del producto

      const cardText = document.createElement('p');
      cardText.className = 'card-text';
      cardText.textContent = data.desc;
      const cardLink = document.createElement('a');
      cardLink.className = 'btn btn-primary';
      cardLink.textContent = 'Ver detalles';
      cardLink.href = '#';

      cardBody.appendChild(cardTitle);
      cardBody.appendChild(cardText);
      cardBody.appendChild(cardLink);

      div.appendChild(img);
      div.appendChild(cardBody);

      document.getElementById("grid").appendChild(div);
    });
  },{
    onlyOnce: true
  });
}

display();


