import { ref, set, onValue, remove, get, child } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-database.js";
import { db, upload } from "./conn.js";
import { getDownloadURL} from "https://www.gstatic.com/firebasejs/10.4.0/firebase-storage.js";

let lastUsedID = 0;

function verificarExistenciaElemento(id) {
  for (let i = 1; i <= id; i++) {
    if (document.getElementById("pro" + i) === null) {
      return i; // Retorna el primer ID faltante
    }
  }
  return id + 1; // Si no hay huecos, retorna el siguiente número
}

function display() {
  const dbRef = ref(db, 'productos');
  let divCount = 0; // Inicializa el contador en 0

  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const key = childSnapshot.key;
      const data = childSnapshot.val();

      const div = document.createElement('div');
      div.className = 'card';
      div.style.width = '18rem';
      div.id = key;
      div.style.marginTop = '20px';

      const img = document.createElement('img');
      img.src = data.foto;
      img.className = 'card-img-top';
      img.alt = data.nombre;
      img.style.height = '250px';

      const cardBody = document.createElement('div');
      cardBody.className = 'card-body';

      const cardTitle = document.createElement('h5');
      cardTitle.className = 'card-title';
      cardTitle.textContent = data.nombre;

      const cardText = document.createElement('p');
      cardText.className = 'card-text';
      cardText.textContent = data.desc;

      const btnM = document.createElement('button');
      btnM.className = 'btn btn-outline-primary';
      btnM.textContent = 'Modificar';
      btnM.style = "margin-right: 10px;";
      btnM.onclick = () => ventanaModificar(key);

      const btnE = document.createElement('button');
      btnE.className = 'btn btn-danger';
      btnE.textContent = 'Eliminar';
      btnE.onclick = () => eliminar(key);

      cardBody.appendChild(cardTitle);
      cardBody.appendChild(cardText);
      cardBody.appendChild(btnM);
      cardBody.appendChild(btnE);

      div.appendChild(img);
      div.appendChild(cardBody);

      document.getElementById("grid").appendChild(div);

      divCount++;
    });

    lastUsedID = verificarExistenciaElemento(divCount); // Actualiza el último ID utilizado

    console.log("Cantidad de divs generados: " + divCount);
  }, {
    onlyOnce: true
  });
}

document.getElementById("guardar").addEventListener("click", function () {
  const fileItem = document.getElementById("imgsubir").files[0];
  const fileName = fileItem.name;
  var imagen = upload(fileItem, fileName);
  var nombre = document.getElementById("nombrea").value;
  var desc = document.getElementById("desca").value;

  setTimeout(() => {
    getDownloadURL(imagen)
      .then((result) => {
        const url = result;
        console.log("URL de descarga del archivo:", url);
        const nuevoID = lastUsedID; // Obtén el próximo ID disponible
        set(ref(db, 'productos/' + "pro" + nuevoID), {
          nombre: nombre,
          desc: desc,
          foto: url
        });
        alert("Producto añadido con éxito");
        location.reload(true);
      })
      .catch((error) => {
        console.error("Error al obtener la URL de descarga:", error);
      });
  }, 5000);
});

function eliminar(llave) {
  console.log(llave);
  remove(ref(db, 'productos/' + llave));
  alert("Elemento removido con éxito");
  location.reload(true);
}

function ventanaModificar(llave){
  const miModal = new bootstrap.Modal(document.getElementById('mod'));
  miModal.show();
  const dbRef = ref(db);
  var nombre = document.getElementById("nombrem");
  var desc = document.getElementById("descm");
  var link;
  get(child(dbRef, `productos/${llave}`)).then((snapshot) => {
    if (snapshot.exists()) {
      nombre.value = snapshot.val().nombre;
      desc.value = snapshot.val().desc;
      link = snapshot.val().foto;
    } else {
      console.log("No data available");
    }
  }).catch((error) => {
    console.error(error);
  });
  
  document.getElementById("modificar").addEventListener("click", function(){
    if(document.getElementById("imgmod").files.length === 0){
      set(ref(db, 'productos/' + llave), {
        nombre: nombre.value,
        desc: desc.value,
        foto: link
      });
      alert("Producto modificado con éxito");
      location.reload(true);
    } else {
      const fileItem = document.getElementById("imgmod").files[0];
      const fileName = fileItem.name;
      var imagen = upload(fileItem, fileName);

      setTimeout(() => {
        getDownloadURL(imagen)
          .then((result) => {
            const url = result;
            console.log("URL de descarga del archivo:", url);
            set(ref(db, 'productos/' + llave), {
              nombre: nombre.value,
              desc: desc.value,
              foto: url
            });
            alert("Producto modificado con éxito");
            location.reload(true);
          })
          .catch((error) => {
            console.error("Error al obtener la URL de descarga:", error);
          });
      }, 5000);
    }
    
  })
}

display();

